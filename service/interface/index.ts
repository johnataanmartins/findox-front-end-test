import { Deal } from "./deal";
import { Document } from "./document";

export interface Service {
  deal: Deal;
  document: Document;
}
