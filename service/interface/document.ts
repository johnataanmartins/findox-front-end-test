export interface Document {
  getAll: () => any;
  getAllByDealIdAndSearch: (dealId: number, search: string) => any;
}
