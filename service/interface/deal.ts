export interface Deal {
  getAll: (
    search?: string,
    filterSaved?: any,
    sortBy?: string,
    sortOrder?: string
  ) => any;
  getFiltersOptions: (
    filter?: string,
    search?: string,
    filterSaved?: any
  ) => any;
}
