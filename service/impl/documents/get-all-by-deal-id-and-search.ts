import { objectToDocument } from "./object-to-document";
import documents from "@/assets/data/documents/documents.json";

export const getAllByDealIdAndSearch = (
  dealId: number,
  search: string
): any => {
  return filterByValue(
    (documents as any).data
      .filter((item: any) => item.deal_id === dealId)
      .map((any: any) => objectToDocument(any)),
    search
  );
};

export const filterByValue = (array: any, search: string = ""): any => {
  return array.filter((item: any) =>
    Object.keys(item).some(
      (k) =>
        !["deal", "posted", "lastAccessed"].includes(k) &&
        item[k].toLowerCase().includes(search.toLowerCase())
    )
  );
};
