import { Document } from "../../interface/document";

import { getAll } from "./get-all";
import { getAllByDealIdAndSearch } from "./get-all-by-deal-id-and-search";

export default {
  getAll,
  getAllByDealIdAndSearch,
} as Document;
