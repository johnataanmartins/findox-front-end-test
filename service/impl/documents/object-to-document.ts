import deals from "@/assets/data/deals/deals.json";

export const objectToDocument = (object: any): any => {
  return {
    id: object.id,
    deal: deals.data.find((item) => item.DealId === object.deal_id),
    name: object.name,
    posted: new Date(object.posted),
    lastAccessed: new Date(object.last_accessed),
    path: object.original_path,
    size: object.blob_size,
  };
};
