import { objectToDocument } from "./object-to-document";
import documents from "@/assets/data/documents/documents.json";

export const getAll = (): any => {
  return (documents as any).data.map((any: any) => objectToDocument(any));
};
