import { Deal } from "../../interface/deal";

import { getAll } from "./get-all";
import { getFiltersOptions } from "./get-filters-options";

export default {
  getAll,
  getFiltersOptions,
} as Deal;
