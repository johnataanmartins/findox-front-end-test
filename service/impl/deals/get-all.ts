import { objectToDeal } from "./object-to-deal";
import deals from "@/assets/data/deals/deals.json";

export const getAll = (
  search: string,
  filterSaved: any,
  sortBy: string,
  sortOrder: string
): any => {
  return filterByValue(
    deals.data.map((any: any) => objectToDeal(any)),
    search
  )
    .filter((element: any) => filterByKey(element, filterSaved))
    .sort((a: any, b: any) =>
      sortOrder === "asc"
        ? a[sortBy].localeCompare(b[sortBy])
        : b[sortBy].localeCompare(a[sortBy])
    );
};

export const filterByValue = (array: any, search: string = ""): any => {
  return array.filter((item: any) =>
    Object.keys(item).some((k) =>
      item[k].toLowerCase().includes(search.toLowerCase())
    )
  );
};

export const filterByKey = (element: any, filter: any): any => {
  let dataFiltered = filter ? false : true;
  let filterEval = "";
  for (const key in filter) {
    if (filter[key].length > 0) {
      const keyText = filter[key][0].key;
      if (filter[key].length > 1) {
        filterEval += `[${filter[key].map(
          (item: any) => `'${item.value}'`
        )}].includes(element.${keyText}) && `;
      } else {
        filterEval += `element.${keyText}.toLowerCase() === '${filter[key][0].value}'.toLowerCase() && `;
      }
    }
  }
  if (
    filterEval.length === 0 ||
    eval(filterEval.substring(0, filterEval.lastIndexOf("&&")))
  ) {
    dataFiltered = true;
  }
  return dataFiltered;
};
