import deals from "@/assets/data/deals/deals.json";
import issuers from "@/assets/data/deals/deals-client-issuers.json";
import industries from "@/assets/data/deals/deals-industries.json";
import agents from "@/assets/data/deals/deals-agents.json";
import sources from "@/assets/data/deals/deals-sources.json";
import types from "@/assets/data/deals/deals-type.json";

export const getFiltersOptions = (
  filter: string,
  search: string,
  filterSaved: any
): any => {
  let data = [];
  switch (filter) {
    case "deals":
      data = deals.data.map((any: any) =>
        objectToFilter(
          any.DealId,
          any.DealName,
          any.DealName,
          "dealName",
          filterSaved
        )
      );
      break;
    case "issuers":
      data = issuers.data.map((any: any) =>
        objectToFilter(
          any.IssuerId,
          any.IssuerName,
          any.IssuerName,
          "issuer",
          filterSaved
        )
      );
      break;
    case "industries":
      data = industries.data.map((any: any) =>
        objectToFilter(
          any.Id,
          any.IndustryName,
          any.IndustryName,
          "industry",
          filterSaved
        )
      );
      break;
    case "access":
      data[0] = objectToFilter("1", "Public", "Public", "access", filterSaved);
      data[1] = objectToFilter(
        "2",
        "Private",
        "Private",
        "access",
        filterSaved
      );
      break;
    case "agents":
      data = agents.data.map((any: any) =>
        objectToFilter(
          any.Id,
          any.CompanyName,
          any.CompanyName,
          "agent",
          filterSaved
        )
      );
      break;
    case "sources":
      data = sources.data.map((any: any) =>
        objectToFilter(
          any.Id,
          any.SourceName,
          any.SourceName,
          "source",
          filterSaved
        )
      );
      break;
    case "types":
      data = types.data.map((any: any) =>
        objectToFilter(any.Id, any.TypeName, any.TypeName, "type", filterSaved)
      );
      break;
  }
  if (search) {
    data = filterByValue(data, search);
  }
  return data.sort((a: any, b: any) =>
    a.active === b.active ? 0 : a.active ? -1 : 1
  );
};

export const objectToFilter = (
  id: string,
  value: string,
  text: string,
  key: string,
  filterSaved: any
): any => {
  let active: boolean = false;
  if (filterSaved) {
    filterSaved.forEach((element: any) => {
      if (element.id === id && element.active) {
        active = true;
      }
    });
  }
  return {
    id: id,
    value: value,
    text: text,
    key: key,
    active: active,
    show: true,
  };
};

export const filterByValue = (array: any, string: string): any => {
  array.forEach(
    (item: any) =>
      (item.show = item.value.toLowerCase().includes(string.toLowerCase()))
  );
  return array;
};
