import issuers from "@/assets/data/deals/deals-client-issuers.json";
import industries from "@/assets/data/deals/deals-industries.json";
import agents from "@/assets/data/deals/deals-agents.json";
import sources from "@/assets/data/deals/deals-sources.json";
import types from "@/assets/data/deals/deals-type.json";

export const objectToDeal = (object: any): any => {
  return {
    id: object.DealId + "",
    dealName: object.DealName,
    issuer:
      issuers.data.find((item) => item.IssuerId === object.IssuerId)
        ?.IssuerName || "-",
    industry:
      industries.data.find((item) => item.Id === object.IndustryId)
        ?.IndustryName || "-",
    access: object.DivisionIsPrivate ? "Private" : "Public",
    agent:
      agents.data.find((item) => item.Id === object.AgentId)?.CompanyName ||
      "-",
    source:
      sources.data.find((item) => item.Id === object.SourceId)?.SourceName ||
      "-",
    type: types.data.find((item) => item.Id === object.TypeId)?.TypeName || "-",
  };
};
