import { Service } from "../interface";

import dealImpl from "./deals";
import DocumentImpl from "./documents";

export const ServiceImpl = {
  deal: dealImpl,
  document: DocumentImpl,
} as Service;
