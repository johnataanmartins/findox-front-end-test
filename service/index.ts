import { Service } from "./interface";
import { ServiceImpl as Impl } from "./impl";

export const ServiceImpl = Impl as Service;
