export default (context: any) => {
  if (context.route.name === "deals") {
    return;
  }
  return context.redirect("/deals");
};
