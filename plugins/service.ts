import { Service } from "@/service/interface";
import { ServiceImpl as Impl } from "@/service/impl";

const ServiceImpl = Impl as Service;

declare module "vue/types/vue" {
  interface Vue {
    $service: Service;
  }
}

export default (_context: any, inject: any) => {
  inject("service", ServiceImpl);
};
