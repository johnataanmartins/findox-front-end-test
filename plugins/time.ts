import Vue from "vue";
import dayjs from "dayjs";

Vue.filter("day_format", function (date: Date, format: string) {
  if (!date) return "";
  if (!(date instanceof Date)) return "";
  return dayjs(date).format(format);
});
