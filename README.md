# Findox Front-end project test

## Project technologies

- Vue JS
- Nuxt JS
- Tailwind CSS for stylesheet

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

Worked on with the node version v16.13.0 and yarn 1.22.17
```

## Project Hosted at https://findox-front-end-test.netlify.app/