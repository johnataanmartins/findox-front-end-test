export default {
  head: {
    title: "findox",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  target: "static",

  css: ["@/assets/css/main.css"],

  router: {
    middleware: "auth",
  },

  plugins: [
    {
      src: "@/plugins/service",
    },
    {
      src: "@/plugins/file-size-format",
    },
    {
      src: "@/plugins/time",
    },
  ],

  components: true,

  buildModules: ["@nuxt/typescript-build", "@nuxtjs/tailwindcss"],

  modules: ["@nuxtjs/axios"],

  axios: {
    baseURL: "/",
  },

  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
  },
};
